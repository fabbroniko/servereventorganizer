package evento.services.friend;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import evento.executor.friend.PendingFriendRequestExecutor;

@WebServlet("/friend/pending")
public class PendingFriendRequest extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		String jsonResponse = new PendingFriendRequestExecutor(request.getReader().readLine()).execute();
		
		response.setContentLength(jsonResponse.length());
		response.setContentType("application/json");
		
		PrintWriter printer = response.getWriter();
		printer.println(jsonResponse);
		printer.close();
	}
}
