package evento.services;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import evento.enums.JSONIdentifier;

@WebServlet("/versions")
public class SupportedVersion extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	private static final int SUPPORTED_CLIENT_VERSION = 2;
	private static final int CURRENT_CLIENT_VERSION = 2;
	private static final int CURRENT_EULA_VERSION = 1;
	
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
		final JSONObject jo = new JSONObject();
		jo.put(JSONIdentifier.SUPPORTED_CLIENT_VERSION.getIdentifier(), SUPPORTED_CLIENT_VERSION);
		jo.put(JSONIdentifier.CURRENT_CLIENT_VERSION.getIdentifier(), CURRENT_CLIENT_VERSION);
		jo.put(JSONIdentifier.CURRENT_EULA_VERSION.getIdentifier(), CURRENT_EULA_VERSION);
		final String jsonResponse = jo.toString();
		
		response.setContentLength(jsonResponse.length());
		response.setContentType("application/json");
		
		PrintWriter printer = response.getWriter();
		printer.println(jsonResponse);
		printer.close();
	}
}
