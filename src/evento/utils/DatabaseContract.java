package evento.utils;

/**
 * Classe astratta che rappresenta i nomi delle tabelle e campi del database.
 * @author fabbroniko
 *
 */
public class DatabaseContract {
	
	public static final String DATABASE_NAME = "grouporganizer";
	public static final String COLUMN_NAME_EXISTS = "my_exists";
	public static final String COLUMN_NAME_LAST_ID = "last_id";

	private DatabaseContract() {}
	
	public static abstract class EventEntry {
		
		public static final String TABLE_NAME = "events";
		public static final String COLUMN_NAME_ID = "id";
		public static final String COLUMN_NAME_NAME = "name";
		public static final String COLUMN_NAME_PLACE = "place";
		public static final String COLUMN_NAME_HOUR = "hour";
		public static final String COLUMN_NAME_MINUTE = "minute";
		public static final String COLUMN_NAME_DESCRIPTION = "description";
	}
	
	public static abstract class BaseUserEntry {
		
		public static final String TABLE_NAME = "base_user";
		public static final String COLUMN_NAME_ID = "id";
		public static final String COLUMN_NAME_FIRST_NAME = "first_name";
		public static final String COLUMN_NAME_LAST_NAME = "last_name";
		public static final String COLUMN_NAME_USERNAME = "username";
		public static final String COLUMN_NAME_TOKEN = "token";
		public static final String COLUMN_NAME_TOKEN_VALIDITY = "token_validity";
		public static final String COLUMN_NAME_NAME_CONFIRMED = "name_confirmed";
	}
	
	public static abstract class FacebookUserEntry {
		
		public static final String TABLE_NAME = "fb_user";
		public static final String COLUMN_NAME_ID = "eo_id";
		public static final String COLUMN_NAME_FACEBOOK_ID = "fb_id";
	}
	
	public static abstract class GoogleUserEntry {
		
		public static final String TABLE_NAME = "google_user";
		public static final String COLUMN_NAME_ID = "eo_id";
		public static final String COLUMN_NAME_GOOGLE_ID = "google_id";
	}
	
	public static abstract class EventAvailabilityEntry {
		
		public static final String TABLE_NAME = "event_availability";
		public static final String COLUMN_NAME_EVENT_ID = "eventID";
		public static final String COLUMN_NAME_USER_ID = "userID";
		public static final String COLUMN_NAME_DATE = "availableDate";
	}
	
	public static abstract class UserEventEntry {
		
		public static final String TABLE_NAME = "user_event";
		public static final String COLUMN_NAME_USER_ID = "user_id";
		public static final String COLUMN_NAME_EVENT_ID = "event_id";
	}
	
	public static abstract class FriendEntry {
		public static final String TABLE_NAME = "friend";
		public static final String FRIEND_REQUEST_VIEW_NAME = "friend_request";
		public static final String CONFIRMED_FIREND_VIEW_NAME = "confirmed_friend";
		public static final String COLUMN_NAME_FIRST_USER = "first_user";
		public static final String COLUMN_NAME_SECOND_USER = "second_user";
		public static final String COLUMN_NAME_CONFIRMED = "confirmed";
	}
	
	public static abstract class GroupEntry {
		
		public static final String TABLE_NAME = "eo_group";
		public static final String COLUMN_NAME_GROUP_ID = "group_id";
		public static final String COLUMN_NAME_GROUP_NAME = "group_name";
	}
	
	public static abstract class UserGroupEntry {
		
		public static final String TABLE_NAME = "user_group";
		public static final String COLUMN_NAME_GROUP_ID = "group_id";
		public static final String COLUMN_NAME_USER_ID = "user_id";
	}
	
	public static abstract class UserCacheEntry {
		
		public static final String TABLE_NAME = "user_cache";
		public static final String COLUMN_NAME_USER_ID = "user_id";
		public static final String COLUMN_NAME_EVENT_LIST = "event_list_last_update";
		public static final String COLUMN_NAME_GROUP_LIST = "group_list_last_update";
		public static final String COLUMN_NAME_FRIEND = "friend_list_last_update";
		public static final String COLUMN_NAME_PENDING_FRIEND = "pending_friend_list_last_update";
	}
	
	public static abstract class EventCacheEntry {
		
		public static final String TABLE_NAME = "event_cache";
		public static final String COLUMN_NAME_EVENT_ID = "event_id";
		public static final String COLUMN_NAME_EVENT_INFO = "event_info";
		public static final String COLUMN_NAME_EVENT_AVAILABILITIES = "event_availabilities";
	}

	public static abstract class GroupCacheEntry {
	
		public static final String TABLE_NAME = "group_cache";
		public static final String COLUMN_NAME_GROUP_ID = "group_id";
		public static final String COLUMN_NAME_GROUP_MEMBERS = "group_members";
	}
	
	public static abstract class BugReport {
		
		public static final String TABLE_NAME = "bug_report";
		public static final String COLUMN_NAME_ID = "id";
		public static final String COLUMN_NAME_USER_ID = "user_id";
		public static final String COLUMN_NAME_TYPE = "type";
		public static final String COLUMN_NAME_FRAGMENT = "fragment";
		public static final String COLUMN_NAME_DETAILS = "details";
	}
	
	public static abstract class Advice {
		
		public static final String TABLE_NAME = "advice";
		public static final String COLUMN_NAME_ID = "id";
		public static final String COLUMN_NAME_USER_ID = "user_id";
		public static final String COLUMN_NAME_DETAILS = "details";
	}
}
