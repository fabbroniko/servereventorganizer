package evento.utils;

public class GoogleResponse {

	private String userID = null;
	private String userFirstName = null;
	private String userLastName = null;
	private int errorCode = -1;
	
	public GoogleResponse(final String userID, final String userFirstName, final String userLastName) {
		this.userID = userID;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
	}
	
	public GoogleResponse(final int errorCode) {
		this.errorCode = errorCode;
	}
	
	public boolean isValid() {
		return (userID != null) && (errorCode == -1);
	}
	
	public String getUserID() {
		return userID;
	}
	
	public String getUserFirstName() {
		return userFirstName;
	}
	
	public String getUserLastName() {
		return userLastName;
	}
	
	public int getErrorCode() {
		return errorCode;
	}
}
