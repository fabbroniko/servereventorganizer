package evento.utils;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Log {
	
	private static final Log MY_INSTANCE = new Log();
	private static final boolean CONSOLE_LOG = true;
	private static final String LOG_FILE_PATH = "/home/EventO/Logs.log";
	
	private SimpleDateFormat smf = null;
	
	private Log() {}

	public static Log getInstance() {
		return MY_INSTANCE;
	}
	
	public synchronized void print(final String message) {
		if(message == null)
			return;
		
		if(smf == null)
			smf = new SimpleDateFormat("dd-MM-yyyy HH:mm:SS");
		
		final String messageWithDateHour = smf.format(new Date()) + ": " + message;
		BufferedOutputStream bufferedOutputStream = null;
		
		if(CONSOLE_LOG)
			System.out.println(messageWithDateHour);
		
		try {
			bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(LOG_FILE_PATH, true));
			bufferedOutputStream.write(messageWithDateHour.getBytes());
			bufferedOutputStream.write("\r\n".getBytes());
		} catch (final FileNotFoundException e) {
			System.out.println("Unable to open the log file: " + messageWithDateHour);
			System.out.println(e.getMessage());
		} catch (final IOException e) {
			System.out.println("Unable to write the log: " + messageWithDateHour);
			e.printStackTrace();
		} finally {
			try {
				if(bufferedOutputStream != null)
					bufferedOutputStream.close();
			} catch (final IOException e) {
				System.out.println("Unable to close the log file.");
				e.printStackTrace();
			}
		}
	}
}
