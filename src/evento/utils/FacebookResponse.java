package evento.utils;

/**
 * Rappresenta la risposta di facebook alla verifica dei dati dell'utente che sta cercando di loggarsi.
 * @author fabbroniko
 *
 */
public class FacebookResponse {

	private String mUserID = null;
	private String firstName = null;
	private String lastName = null;
	private int errorCode = -1;
	
	/**
	 * Risposta positiva da parte di facebook. Costruisce l'oggetto con nome e cognome dell'utente.
	 * @param firstName Nome dell'utente.
	 * @param lastName Cognome dell'utente.
	 */
	public FacebookResponse(final String userID, final String firstName, final String lastName) {
		mUserID = userID;
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	/**
	 * Risposta negativa. Autenticazione fallita.
	 */
	public FacebookResponse(final int errorCode) {
		this.errorCode = errorCode;
	}
	
	/**
	 * Controlla se l'autenticazione � avvenuta con successo o meno.
	 * @return Restituisce true se � avvenuta con successo o false in caso contrario.
	 */
	public boolean isValid() {
		return errorCode == -1;
	}
	
	public String getUserID() {
		return mUserID;
	}
	
	/**
	 * Getter del nome dell'utente prelevato dalla richiesta a facebook.
	 * @return Restituisce il nome dell'utente.
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * Getter del cognome dell'utente prelevato dalla richiesta a facebook.
	 * @return Restituisce il cognome dell'utente.
	 */
	public String getLastName() {
		return lastName;
	}
	
	public int getErrorCode() {
		return this.errorCode;
	}
}
