package evento.enums;

public enum JSONIdentifier {

	FULL_NAME("full_name"),
	
	EVENT_ID("event_id"),
	
	EVENT_NAME("event_name"),
	
	GENERAL_TOKEN("token"),
	
	GENERAL_ID("id"),
	
	ERROR_CODE("error_code"),
	
	FIRST_NAME("first_name"),
	
	LAST_NAME("last_name"),
	
	CODE("code"),
	
	FACEBOOK_TOKEN("fb_token"),
	
	PLACE("place"),
	
	DATES("dates"),
	
	DATE("date"),

	GOOGLE_TOKEN("google_token"),
	
	GOOGLE_FIRST_NAME("given_name"),
	
	GOOGLE_LAST_NAME("family_name"),
	
	GOOGLE_OAUTH_CLIENT_ID("aud"),
	
	GOOGLE_USER_ID("sub"),
	
	PARTICIPANTS("participants"),
	
	EVENT_LIST("events"),
	
	GROUP_LIST("groups"),
	
	USERNAME("username"),
	
	CONFIRMED_FRIENDS("confirmed_friends"),
	
	PENDING_FRIENDS("pending_friends"),
	
	GROUP_ID("group_id"),
	
	GROUP_NAME("group_name"),
	
	USERNAME_LIST("username_list"),
	
	LAST_UPDATE("last_update"),
	
	HOUR("hour"),
	
	MINUTE("minute"),
	
	DESCRIPTION("description"),
	
	BUG_TYPE("bug_type"),

	BUG_FRAGMENT("bug_fragment"),

	BUG_DETAILS("bug_details"),

	ADVICE_DETAILS("advice_details"),
	
	SUPPORTED_CLIENT_VERSION("supported_client_version"),
	
	CURRENT_CLIENT_VERSION("current_client_version"),
	
	CURRENT_EULA_VERSION("eula_version");
	
	final String identifier;
	
	private JSONIdentifier(final String identifier) {
		this.identifier = identifier;
	}
	
	public String getIdentifier() {
		return identifier;
	}
}
