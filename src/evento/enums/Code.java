package evento.enums;

public enum Code {
	SUCCESS(0),
	
	NAME_CONF_REQUIRED(1),
	
	TOKEN_UPDATED(2),
	
	REQUEST(3),
	
	CANCEL(4),
	
	APPROVED(5);
	
	int code;
	
	private Code(final int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
