package evento.enums;

/**
 * Rappresenta i possibili codici di errore che il server pu� mandare a seguito di una richiesta del client.
 * @author fabbroniko
 *
 */
public enum ErrorCode {

	/**
	 * Codice di errore relativo ad una richiesta di un client mal formattata (ad esempio non se la richiesta non � in JSON).
	 */
	BAD_REQUEST(0),
	
	/**
	 * Il formato del nome inviato dal client non rispetta le caratteristiche di lunghezza richieste.
	 */
	USER_NAME_FORMAT_NOT_VALID(1),
	
	/**
	 * Il formato del nome dell'evento inviato dal client non rispetta le caratteristiche di lunghezza richieste.
	 */
	EVENT_NAME_FORMAT_NOT_VALID(2),
	
	/**
	 * Il formato della password dell'evento inviato dal client non rispetta le caratteristiche di lunghezza richieste.
	 */
	EVENT_PASSWORD_FORMAT_NOT_VALID(3),
	
	/**
	 * Il formato del luogo dell'evento non � valido.
	 */
	EVENT_PLACE_NOT_VALID(4),
	
	/**
	 * L'ID dell'evento richiesto dal client non esiste oppure � stato eliminato.
	 */
	EVENT_ID_DOESNT_EXIST(5),
	
	/**
	 * La password dell'evento immessa non � valida. L'utente non pu� unirsi all'evento.
	 */
	EVENT_WRONG_PASSWORD(6),
	
	USERNAME_NOT_VALID(7),
	
	/**
	 * Le date inserite nella richiesta non sono valide.
	 */
	DATES_NOT_VALID(8),
	
	/**
	 * Impossibile completare la richiesta dell'utente poich� questo non risulta unito all'evento richiesto.
	 */
	NO_SUCH_EVENT_USER_ASSOC(9),
	
	NAME_NOT_CONFIRMED(10),
	
	/**
	 * Uno o pi� campi richiesti per una specifica richiesta non sono presenti.
	 */
	MISSING_FIELD(11),
	
	/**
	 * Autenticazione attraverso i server di facebook non riuscita. Il token non � valido oppure non appartiene all'utente con ID specificato oppure il token non � stato generato per questa applicazione.
	 */
	INVALID_FACEBOOK_LOGIN(12),
	
	/**
	 * Autenticazioen con ID e Token fallita.
	 */
	AUTH_FAILED(14),
	
	GOOGLE_INVALID_LOGIN(15),
	
	GOOGLE_INVALID_OAUTH_CLIENT_ID(16),
	
	USERNAME_EXISTS(17),
	
	USERNAME_DOESNT_EXIST(18),
	
	NO_SUCH_USER_GROUP_ASSOCIATION(19),
	
	/**
	 * Errore interno. Impossibile stabilire una connessione con il database.
	 */
	SERVER_DATABASE_CONNECTION_FAILED(1000),
	
	/**
	 * Errore interno. Eccezione MySQL.
	 */
	SERVER_SQL_EXCEPTION(1001),
	
	/**
	 * Errore interno. La risposta arrivata dai server di facebook non � valida oppure � formattata diversamente da quando aspettato.
	 */
	SERVER_FACEBOOK_REPLY_ERROR(1002),
	
	/**
	 * Errore interno. Impossibile stabilire una connessione con i server facebook.
	 */
	SERVER_FACEBOOK_CONNECTION_ISSUE(1003),
	
	SERVER_GOOGLE_REPLY_EXCEPTION(1004),	
	
	SERVER_GOOGLE_CONNECTION_ISSUE(1005);
	
	private int code;
	
	private ErrorCode(final int code) {
		this.code = code;
	}
	
	public int getCode() {
		return code;
	}
}
