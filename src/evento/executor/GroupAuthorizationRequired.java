package evento.executor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import evento.utils.DatabaseContract;

public abstract class GroupAuthorizationRequired extends AuthenticationRequired {

	protected GroupAuthorizationRequired(final String jsonRequest) {
		super(jsonRequest);
	}
	
	protected final boolean checkUserGroupAssociation(final int userID, final int groupID) throws SQLException {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement associationQuery = conn.prepareStatement("SELECT COUNT(1) AS " + 
		DatabaseContract.COLUMN_NAME_EXISTS + 
		" FROM " + DatabaseContract.UserGroupEntry.TABLE_NAME + 
		" WHERE " + DatabaseContract.UserGroupEntry.COLUMN_NAME_USER_ID + 
		" = ? AND " + DatabaseContract.UserGroupEntry.COLUMN_NAME_GROUP_ID + " = ?");
		
		associationQuery.setInt(1, userID);
		associationQuery.setInt(2, groupID);
		ResultSet associationQueryResult = associationQuery.executeQuery();
		
		if(!associationQueryResult.next())
			return false;
		
		return (associationQueryResult.getInt(DatabaseContract.COLUMN_NAME_EXISTS) == 1);
	}
}
