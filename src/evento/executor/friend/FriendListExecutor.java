package evento.executor.friend;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;
import evento.utils.DatabaseContract.FriendEntry;

public class FriendListExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	
	public FriendListExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement getFriendList = conn.prepareStatement("SELECT " + 
					BaseUserEntry.COLUMN_NAME_ID + ", " + 
					BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", " + 
					BaseUserEntry.COLUMN_NAME_LAST_NAME + ", " + 
					BaseUserEntry.COLUMN_NAME_USERNAME + " FROM " + 
					BaseUserEntry.TABLE_NAME + " WHERE " + 
					BaseUserEntry.COLUMN_NAME_ID + " IN (SELECT CASE WHEN " + 
					FriendEntry.COLUMN_NAME_FIRST_USER + "=? THEN " + 
					FriendEntry.COLUMN_NAME_SECOND_USER + " ELSE " + 
					FriendEntry.COLUMN_NAME_FIRST_USER + " END AS friend_id FROM " + 
					FriendEntry.CONFIRMED_FIREND_VIEW_NAME + " WHERE " + 
					FriendEntry.COLUMN_NAME_FIRST_USER + "=? OR " + FriendEntry.COLUMN_NAME_SECOND_USER + "=?)");
			getFriendList.setInt(1, id);
			getFriendList.setInt(2, id);
			getFriendList.setInt(3, id);
			ResultSet getFriendListResult = getFriendList.executeQuery();
			
			final JSONArray jsonArray = new JSONArray();
			
			while(getFriendListResult.next()) {
				final JSONObject tmpObj = new JSONObject();
				tmpObj.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getFriendListResult.getInt(BaseUserEntry.COLUMN_NAME_ID));
				tmpObj.put(JSONIdentifier.FULL_NAME.getIdentifier(), getFriendListResult.getString(BaseUserEntry.COLUMN_NAME_FIRST_NAME) + " " + getFriendListResult.getString(BaseUserEntry.COLUMN_NAME_LAST_NAME));
				tmpObj.put(JSONIdentifier.USERNAME.getIdentifier(), getFriendListResult.getString(BaseUserEntry.COLUMN_NAME_USERNAME));
				
				jsonArray.put(tmpObj);
			}
			
			retValue.put(JSONIdentifier.CONFIRMED_FRIENDS.getIdentifier(), jsonArray);
		} catch (final SQLException e) {
			Log.getInstance().print("[FriendListExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
