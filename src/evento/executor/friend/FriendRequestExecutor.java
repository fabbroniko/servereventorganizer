package evento.executor.friend;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public class FriendRequestExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	private String username;
	private int code;
	
	public FriendRequestExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.USERNAME.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.CODE.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				username = jsonObject.getString(JSONIdentifier.USERNAME.getIdentifier());
				code = jsonObject.getInt(JSONIdentifier.CODE.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		if((code != Code.REQUEST.getCode()) && (code != Code.CANCEL.getCode()))
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
			
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
		
			PreparedStatement getUsernameID = conn.prepareStatement("SELECT @username_id:=" + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + " AS " + DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + ", " +
					DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME + 
					" FROM " + DatabaseContract.BaseUserEntry.TABLE_NAME + 
					" WHERE " + DatabaseContract.BaseUserEntry.COLUMN_NAME_USERNAME + "=?");
			getUsernameID.setString(1, username);
			ResultSet getUsernameIDResult = getUsernameID.executeQuery();
			
			if(!getUsernameIDResult.next()) {
				return buildErrorResponse(ErrorCode.USERNAME_DOESNT_EXIST.getCode());
			} else {
				if(id == getUsernameIDResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID))
					return buildErrorResponse(ErrorCode.USERNAME_NOT_VALID.getCode());
			}
			
			PreparedStatement checkFriendshipConfirmed = conn.prepareStatement("SELECT COUNT(*) as " + 
					DatabaseContract.COLUMN_NAME_EXISTS + " FROM " + 
					DatabaseContract.FriendEntry.CONFIRMED_FIREND_VIEW_NAME + " WHERE (" + 
					DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=@username_id AND " + 
					DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=?) OR  (" + 
					DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=? AND " + 
					DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=@username_id)");
			checkFriendshipConfirmed.setInt(1, id);
			checkFriendshipConfirmed.setInt(2, id);
			ResultSet checkFriendshipConfirmedResult = checkFriendshipConfirmed.executeQuery();
			
			checkFriendshipConfirmedResult.next();
			if(checkFriendshipConfirmedResult.getInt(DatabaseContract.COLUMN_NAME_EXISTS) == 1) {
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.APPROVED.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUsernameIDResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID));
				retValue.put(JSONIdentifier.FULL_NAME.getIdentifier(), getUsernameIDResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME) + " " + getUsernameIDResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME));
				retValue.put(JSONIdentifier.USERNAME.getIdentifier(), username);
			} else {
				PreparedStatement checkFriendshipRequest = conn.prepareStatement("SELECT " + 
						DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + ", " +
						DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + " FROM " +
						DatabaseContract.FriendEntry.FRIEND_REQUEST_VIEW_NAME + " WHERE (" + 
						DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=@username_id AND " + 
						DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=?) OR  (" + 
						DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=? AND " + 
						DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=@username_id)");
				checkFriendshipRequest.setInt(1, id);
				checkFriendshipRequest.setInt(2, id);
				ResultSet checkFriendshipRequestResult = checkFriendshipRequest.executeQuery();
				
				if(checkFriendshipRequestResult.next()) {
					if(checkFriendshipRequestResult.getInt(DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER) == id) {
						retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
					} else {
						if(code == Code.REQUEST.getCode()) {
							final PreparedStatement confirmFriendship = conn.prepareStatement("UPDATE " + 
								DatabaseContract.FriendEntry.TABLE_NAME + " SET " +
								DatabaseContract.FriendEntry.COLUMN_NAME_CONFIRMED + "=? WHERE " +
								DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=@username_id AND " +
								DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=?");
							confirmFriendship.setBoolean(1, true);
							confirmFriendship.setInt(2, id);
							confirmFriendship.executeUpdate(); 
							
							retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.APPROVED.getCode());
							retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUsernameIDResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID));
							retValue.put(JSONIdentifier.FULL_NAME.getIdentifier(), getUsernameIDResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME) + " " + getUsernameIDResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME));
							retValue.put(JSONIdentifier.USERNAME.getIdentifier(), username);
						} else {
							final PreparedStatement deleteFriendshipRequest = conn.prepareStatement("DELETE FROM " + 
									DatabaseContract.FriendEntry.TABLE_NAME + " WHERE " +
									DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + "=@username_id AND " +
									DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + "=?");
							deleteFriendshipRequest.setInt(1, id);
							deleteFriendshipRequest.executeUpdate(); 
							
							retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
						}
					}
				} else {
					final PreparedStatement insertNewFriendRequest = conn.prepareStatement("INSERT INTO " + 
							DatabaseContract.FriendEntry.TABLE_NAME + " (" +
							DatabaseContract.FriendEntry.COLUMN_NAME_FIRST_USER + ", " +
							DatabaseContract.FriendEntry.COLUMN_NAME_SECOND_USER + ", " +
							DatabaseContract.FriendEntry.COLUMN_NAME_CONFIRMED + ") VALUES (?, @username_id, ?)");
					insertNewFriendRequest.setInt(1, id);
					insertNewFriendRequest.setBoolean(2, false);
					insertNewFriendRequest.executeUpdate();
					
					retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
				}
			}
			
		} catch (final SQLException e) {
			Log.getInstance().print("[FriendRequestExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
