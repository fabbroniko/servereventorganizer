package evento.executor;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.json.JSONObject;

import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public abstract class AbstractExecutor {

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver").newInstance();
		} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			Log.getInstance().print("Impossibile registrare il driver per il database mysql.");
			Log.getInstance().print("[AbstractExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
		}
	}
	
	private static final String MYSQL_USER = "root";
	private static final String MYSQL_PASSWORD = "sharedwithfede";
	
	protected Connection conn;
	protected String jsonRequest;
	
	protected AbstractExecutor(final String jsonRequest) {
		this.jsonRequest = jsonRequest;
	}
	
	protected final void connect() throws DatabaseConnectionException {
		try {
			conn = DriverManager.getConnection("jdbc:mysql://localhost/" + DatabaseContract.DATABASE_NAME + "?useSSL=false", MYSQL_USER, MYSQL_PASSWORD);
		} catch (SQLException e) {
			Log.getInstance().print("[AbstractExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			throw new DatabaseConnectionException();
		}
	}
	
	public abstract String execute();
	
	protected final void disconnect() {
		if (isConnectionOpen()) {
			try {
				conn.close();
			} catch (final SQLException e) {
				Log.getInstance().print("[AbstractExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			}
		}
	}
	
	protected final boolean isConnectionOpen() {
		try {
			return conn != null && !conn.isClosed();
		} catch (final SQLException e) {
			return false;
		}
	}
	
	/**
	 * Utility di generazione della stringa JSON a partire dal codice dell'errore.
	 * @param errorCode
	 * @return
	 */
	protected static String buildErrorResponse(final int errorCode) {
		final JSONObject jsonError = new JSONObject();
		jsonError.put(JSONIdentifier.ERROR_CODE.getIdentifier(), errorCode);
		return jsonError.toString();
	}
	
	/*
	protected final void updateUserCache(final int userID, final long timestamp, final String columnName) throws SQLException  {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement updateCacheQuery = conn.prepareStatement("UPDATE " + 
				UserCacheEntry.TABLE_NAME + " SET " + 
				columnName + "=? WHERE " + 
				UserCacheEntry.COLUMN_NAME_USER_ID + "=?");
		
		updateCacheQuery.setTimestamp(1, new Timestamp(timestamp));
		updateCacheQuery.setInt(2, userID);
		updateCacheQuery.executeUpdate();
	}

	protected final void updateEventCache(final int eventID, final long timestamp, final String columnName) throws SQLException  {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement updateCacheQuery = conn.prepareStatement("UPDATE " + 
				EventCacheEntry.TABLE_NAME + " SET " + 
				columnName + "=? WHERE " + 
				EventCacheEntry.COLUMN_NAME_EVENT_ID + "=?");
		
		updateCacheQuery.setTimestamp(1, new Timestamp(timestamp));
		updateCacheQuery.setInt(2, eventID);
		updateCacheQuery.executeUpdate();
	}

	protected final void updateGroupCache(final int groupID, final long timestamp, final String columnName) throws SQLException  {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement updateCacheQuery = conn.prepareStatement("UPDATE " + 
				GroupCacheEntry.TABLE_NAME + " SET " + 
				columnName + "=? WHERE " + 
				GroupCacheEntry.COLUMN_NAME_GROUP_ID + "=?");
		
		updateCacheQuery.setTimestamp(1, new Timestamp(timestamp));
		updateCacheQuery.setInt(2, groupID);
		updateCacheQuery.executeUpdate();
	}
	*/
}
