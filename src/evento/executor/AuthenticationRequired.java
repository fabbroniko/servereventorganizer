package evento.executor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import evento.exception.NameNotConfirmedException;
import evento.utils.DatabaseContract.BaseUserEntry;

public abstract class AuthenticationRequired extends AbstractExecutor {

	protected AuthenticationRequired(final String jsonRequest) {
		super(jsonRequest);
	}
	
	protected final boolean authenticate(final int id, final String token) throws SQLException, NameNotConfirmedException {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement credentialsQuery = conn.prepareStatement("SELECT " + 
		BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED + " FROM " + 
		BaseUserEntry.TABLE_NAME + " WHERE " + 
		BaseUserEntry.COLUMN_NAME_ID + " = ? AND " + 
		BaseUserEntry.COLUMN_NAME_TOKEN + " = ?");
		
		credentialsQuery.setInt(1, id);
		credentialsQuery.setString(2, token);
		ResultSet credentialsQueryResult = credentialsQuery.executeQuery();
		
		if(!credentialsQueryResult.next())
			return false;
		
		if(!credentialsQueryResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED))
			throw new NameNotConfirmedException();
		
		return true;
	}
}
