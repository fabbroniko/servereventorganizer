package evento.executor;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import evento.utils.DatabaseContract;

public abstract class EventAuthorizationRequired extends AuthenticationRequired {

	protected EventAuthorizationRequired(final String jsonRequest) {
		super(jsonRequest);
	}

	protected final boolean checkUserEventAssociation(final int userID, final int eventID) throws SQLException {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement associationQuery = conn.prepareStatement("SELECT COUNT(1) AS " + 
		DatabaseContract.COLUMN_NAME_EXISTS + 
		" FROM " + DatabaseContract.UserEventEntry.TABLE_NAME + 
		" WHERE " + DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + 
		" = ? AND " + DatabaseContract.UserEventEntry.COLUMN_NAME_EVENT_ID + " = ?");
		
		associationQuery.setInt(1, userID);
		associationQuery.setInt(2, eventID);
		ResultSet associationQueryResult = associationQuery.executeQuery();
		
		if(!associationQueryResult.next())
			return false;
		
		return (associationQueryResult.getInt(DatabaseContract.COLUMN_NAME_EXISTS) == 1);
	}
}
