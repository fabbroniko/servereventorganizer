package evento.executor;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import evento.utils.DatabaseContract.BaseUserEntry;

public abstract class LoginRequest extends AbstractExecutor {

	private static SecureRandom random = new SecureRandom();
	 
	protected LoginRequest(final String jsonRequest) {
		super(jsonRequest);
	}

	protected final void updateToken(final int userID, final String userToken) throws SQLException {
		if(!isConnectionOpen()){
			throw new IllegalStateException();
		}
		
		PreparedStatement updateTokenQuery = conn.prepareStatement("UPDATE " + 
				BaseUserEntry.TABLE_NAME + " SET " + 
				BaseUserEntry.COLUMN_NAME_TOKEN + "=? WHERE " + 
				BaseUserEntry.COLUMN_NAME_ID + "=?");
		
		updateTokenQuery.setString(1, userToken);
		updateTokenQuery.setInt(2, userID);
		updateTokenQuery.executeUpdate();
	}
	
	/**
	 * Utility di generazione dei token da utilizzare per i client in fase di login.
	 * @return Restituisce un token.
	 */
	public static String generateToken() {
		return new BigInteger(130, random).toString(32);
	}
}
