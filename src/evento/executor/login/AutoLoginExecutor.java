package evento.executor.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.executor.LoginRequest;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;

public class AutoLoginExecutor extends AuthenticationRequired {

	private static final long ONE_SECOND = 1000;
	private static final long ONE_MINUTE = ONE_SECOND * 60;
	private static final long ONE_HOUR = ONE_MINUTE * 60;
	private static final long ONE_DAY = ONE_HOUR * 24;
	private static final long TOKEN_VALIDITY_OFFSET = ONE_DAY * 30;
	
	private int id;
	private String token;
	
	public AutoLoginExecutor(final String jsonRequest) {
		super(jsonRequest);
	}
	
	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		return executeQuery();
	}
	
	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		long validityTimestamp;
		final long requestTimestamp = System.currentTimeMillis();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement getTokenValidity = conn.prepareStatement("SELECT " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_TOKEN_VALIDITY + " FROM " +
					DatabaseContract.BaseUserEntry.TABLE_NAME + " WHERE " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + "=?");
			
			getTokenValidity.setInt(1, id);
			ResultSet tokenValidityResult = getTokenValidity.executeQuery();
			
			if(!tokenValidityResult.next())
				return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
				
			validityTimestamp = tokenValidityResult.getTimestamp(DatabaseContract.BaseUserEntry.COLUMN_NAME_TOKEN_VALIDITY).getTime();
			
			if(requestTimestamp > validityTimestamp) {
				String newToken = LoginRequest.generateToken();
				
				PreparedStatement updateTokenQuery = conn.prepareStatement("UPDATE " + 
						BaseUserEntry.TABLE_NAME + " SET " + 
						BaseUserEntry.COLUMN_NAME_TOKEN + "=?, " + 
						BaseUserEntry.COLUMN_NAME_TOKEN_VALIDITY + "=? WHERE " + 
						BaseUserEntry.COLUMN_NAME_ID + "=?");
				
				updateTokenQuery.setString(1, newToken);
				updateTokenQuery.setTimestamp(2, new Timestamp(requestTimestamp + TOKEN_VALIDITY_OFFSET));
				updateTokenQuery.setInt(3, id);
				updateTokenQuery.executeUpdate();
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.TOKEN_UPDATED.getCode());
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), newToken);
			} else {
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[AutoLoginExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}

}
