package evento.executor.login;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;

public class ConfirmUserDataExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	private String firstName;
	private String lastName;
	private String username;
	
	private static final int MIN_SUB_USER_NAME_LENGTH = 1;
    private static final int MAX_SUB_USER_NAME_LENGTH = 30;
    private static final int MAX_USERNAME_CHARS = 30;
	private static final int MIN_USERNAME_CHARS = 5;
	
	public ConfirmUserDataExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.FIRST_NAME.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.LAST_NAME.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.USERNAME.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				firstName = jsonObject.getString(JSONIdentifier.FIRST_NAME.getIdentifier());
				lastName = jsonObject.getString(JSONIdentifier.LAST_NAME.getIdentifier());
				username = jsonObject.getString(JSONIdentifier.USERNAME.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		if(!isSubUserNameValid(firstName) || !isSubUserNameValid(lastName)) {
			return buildErrorResponse(ErrorCode.USER_NAME_FORMAT_NOT_VALID.getCode());
		}
		
		if(!isUsernameValid(username)) 
			return buildErrorResponse(ErrorCode.USERNAME_NOT_VALID.getCode());
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			try {
				if(!authenticate(id, token)) {
					return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
				}
			} catch (final NameNotConfirmedException e) {}
			
			PreparedStatement checkQuery = conn.prepareStatement("SELECT " + 
					BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED + " FROM " + 
					BaseUserEntry.TABLE_NAME + " WHERE " + 
					BaseUserEntry.COLUMN_NAME_ID + "=?");
			
			checkQuery.setInt(1, id);
			ResultSet checkQueryResult = checkQuery.executeQuery();
			
			if(checkQueryResult.next()) {
				if(!checkQueryResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED)) {
					PreparedStatement updateNameQuery = conn.prepareStatement("UPDATE " + 
							BaseUserEntry.TABLE_NAME + " SET " + 
							BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED + "=?, " +
							BaseUserEntry.COLUMN_NAME_USERNAME + "=?, " + 
							BaseUserEntry.COLUMN_NAME_FIRST_NAME + "=?, " + 
							BaseUserEntry.COLUMN_NAME_LAST_NAME + "=? WHERE " + 
							BaseUserEntry.COLUMN_NAME_ID + "=?");
					
					updateNameQuery.setBoolean(1, true);
					updateNameQuery.setString(2, username);
					updateNameQuery.setString(3, firstName);
					updateNameQuery.setString(4, lastName);
					updateNameQuery.setInt(5, id);
					
					try {
						updateNameQuery.executeUpdate();
					} catch (final SQLException e) {
						return buildErrorResponse(ErrorCode.USERNAME_EXISTS.getCode());
					}
				}
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[ConfirmUserDataExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
	
	private static boolean isUsernameValid(final String username) {
		final String trimmedUsername = username != null ? username.trim() : null;
		return (trimmedUsername != null) && (trimmedUsername.length() >= MIN_USERNAME_CHARS) && (trimmedUsername.length() <= MAX_USERNAME_CHARS);
	}
	
	private static boolean isSubUserNameValid(final String value) {
		return (value != null) && (value.length() >= MIN_SUB_USER_NAME_LENGTH) && (value.length() <= MAX_SUB_USER_NAME_LENGTH);
	}
}
