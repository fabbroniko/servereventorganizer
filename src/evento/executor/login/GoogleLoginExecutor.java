package evento.executor.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.executor.LoginRequest;
import evento.utils.DatabaseContract;
import evento.utils.GoogleResponse;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;
import evento.utils.DatabaseContract.GoogleUserEntry;

public class GoogleLoginExecutor extends LoginRequest {

	private static final String OAUTH_CLIENT_ID = "519333593971-5tkjcmjam4f1psmsslgclkk3io44234r.apps.googleusercontent.com";
	
	private String googleToken; 
	
	public GoogleLoginExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GOOGLE_TOKEN.getIdentifier())) {
				googleToken = jsonObject.getString(JSONIdentifier.GOOGLE_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		return executeQuery();
	}

	private String executeQuery() {
		final GoogleResponse googleResponse = checkGoogleToken(googleToken);
		if(!googleResponse.isValid()) {
			return buildErrorResponse(googleResponse.getErrorCode());
		}
		
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			PreparedStatement getUserInformationQuery = conn.prepareStatement("SELECT b." + 
					BaseUserEntry.COLUMN_NAME_ID + ", b." +  
					BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_LAST_NAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_USERNAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED + " FROM " + 
					BaseUserEntry.TABLE_NAME + " b JOIN " + 
					GoogleUserEntry.TABLE_NAME + " f ON b." + 
					BaseUserEntry.COLUMN_NAME_ID + "=f." + 
					GoogleUserEntry.COLUMN_NAME_ID + " WHERE f." + 
					GoogleUserEntry.COLUMN_NAME_GOOGLE_ID + "=?");
			
			getUserInformationQuery.setString(1, googleResponse.getUserID());
			ResultSet getUserInformationResult = getUserInformationQuery.executeQuery();
			
			final String generatedToken = LoginRequest.generateToken();
			
			if(!getUserInformationResult.next()) { 
				PreparedStatement newUserQuery = conn.prepareStatement("INSERT INTO " + BaseUserEntry.TABLE_NAME + " (" + BaseUserEntry.COLUMN_NAME_TOKEN + ") VALUES (?)");
				newUserQuery.setString(1, generatedToken);
				newUserQuery.executeUpdate();
				
				PreparedStatement getNewUserIDQuery = conn.prepareStatement("SELECT LAST_INSERT_ID() AS " + DatabaseContract.COLUMN_NAME_LAST_ID);
				ResultSet getNewUserIDQueryResult = getNewUserIDQuery.executeQuery();
				
				if(!getNewUserIDQueryResult.next())
					return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
				
				/*
				PreparedStatement newUserCache = conn.prepareStatement("INSERT INTO " + UserCacheEntry.TABLE_NAME + " (" + UserCacheEntry.COLUMN_NAME_USER_ID + ") VALUES (?)");
				newUserCache.setInt(1, getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				newUserCache.executeUpdate();
				*/
				
				PreparedStatement newGoogleUserQuery = conn.prepareStatement("INSERT INTO " + 
						GoogleUserEntry.TABLE_NAME + " (" + 
						GoogleUserEntry.COLUMN_NAME_ID + ", " + 
						GoogleUserEntry.COLUMN_NAME_GOOGLE_ID + ") VALUES (?, ?)");
				
				newGoogleUserQuery.setInt(1, getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				newGoogleUserQuery.setString(2, googleResponse.getUserID());
				newGoogleUserQuery.executeUpdate();
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.NAME_CONF_REQUIRED.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				if(googleResponse.getUserFirstName() != null)
					retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), googleResponse.getUserFirstName());
				if(googleResponse.getUserLastName() != null)
					retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), googleResponse.getUserLastName());
			} else if(getUserInformationResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED)) {
				updateToken(getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID), generatedToken);
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_FIRST_NAME));
				retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_LAST_NAME));
				retValue.put(JSONIdentifier.USERNAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_USERNAME));
			} else if(!getUserInformationResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED)) {
				updateToken(getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID), generatedToken);
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.NAME_CONF_REQUIRED.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				if(googleResponse.getUserFirstName() != null)
					retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), googleResponse.getUserFirstName());
				if(googleResponse.getUserLastName() != null)
					retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), googleResponse.getUserLastName());
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[GoogleLoginExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
	
	private static GoogleResponse checkGoogleToken(final String token) {
		HttpsURLConnection googleConnection = null;
		
		try {
			final URL facebookURL = new URL("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + token);
			googleConnection = (HttpsURLConnection)facebookURL.openConnection();
			googleConnection.setRequestMethod("GET");
			googleConnection.setUseCaches(false);
			googleConnection.setDoOutput(false);
			googleConnection.connect();

			if (googleConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return new GoogleResponse(ErrorCode.GOOGLE_INVALID_LOGIN.getCode());
			}

			final InputStream is = googleConnection.getInputStream();
            final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            final StringBuilder response = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            googleConnection.disconnect();

            final JSONObject jsonObject = new JSONObject(response.toString());
            if(!jsonObject.has(JSONIdentifier.GOOGLE_USER_ID.getIdentifier()) || !jsonObject.has(JSONIdentifier.GOOGLE_OAUTH_CLIENT_ID.getIdentifier())) {
            	return new GoogleResponse(ErrorCode.SERVER_GOOGLE_REPLY_EXCEPTION.getCode());
            }

            final String respOauthClientID = jsonObject.getString(JSONIdentifier.GOOGLE_OAUTH_CLIENT_ID.getIdentifier());

            if(respOauthClientID.equals(OAUTH_CLIENT_ID)) {
            	final String userID = jsonObject.getString(JSONIdentifier.GOOGLE_USER_ID.getIdentifier());
            	final String firstName = (jsonObject.has(JSONIdentifier.GOOGLE_FIRST_NAME.getIdentifier()) ? jsonObject.getString(JSONIdentifier.GOOGLE_FIRST_NAME.getIdentifier()) : null);
                final String lastName = (jsonObject.has(JSONIdentifier.GOOGLE_LAST_NAME.getIdentifier()) ? jsonObject.getString(JSONIdentifier.GOOGLE_LAST_NAME.getIdentifier()) : null);

            	return new GoogleResponse(userID, firstName, lastName);
            } else {
            	return new GoogleResponse(ErrorCode.GOOGLE_INVALID_OAUTH_CLIENT_ID.getCode());
            }
		} catch (final ParseException e) {
			return new GoogleResponse(ErrorCode.SERVER_GOOGLE_REPLY_EXCEPTION.getCode());
		} catch (final IOException e) {
			return new GoogleResponse(ErrorCode.SERVER_GOOGLE_CONNECTION_ISSUE.getCode());
		} finally {
			if(googleConnection != null)
				googleConnection.disconnect();
		}
	}
}
