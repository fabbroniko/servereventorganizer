package evento.executor.login;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.AppSecretProofException;
import evento.exception.DatabaseConnectionException;
import evento.executor.LoginRequest;
import evento.utils.DatabaseContract;
import evento.utils.FacebookResponse;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;
import evento.utils.DatabaseContract.FacebookUserEntry;

public class FacebookLoginExecutor extends LoginRequest {

	private static final String APP_SECRET_FACEBOOK = "819690872e0f7c08838a47d11002d030";
	
	private String fbToken;
	
	public FacebookLoginExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.FACEBOOK_TOKEN.getIdentifier())) {
				fbToken = jsonObject.getString(JSONIdentifier.FACEBOOK_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		return executeQuery();
	}

	private String executeQuery() {
		final FacebookResponse fbResponse = checkFacebookData(fbToken);
		if(!fbResponse.isValid()) {
			return buildErrorResponse(fbResponse.getErrorCode());
		}
		
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			PreparedStatement getUserInformationQuery = conn.prepareStatement("SELECT b." + 
					BaseUserEntry.COLUMN_NAME_ID + ", b." + 
					BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_LAST_NAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_USERNAME + ", b." + 
					BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED + " FROM " + 
					BaseUserEntry.TABLE_NAME + " b JOIN " + 
					FacebookUserEntry.TABLE_NAME + " f ON b." + 
					BaseUserEntry.COLUMN_NAME_ID + "=f." + 
					FacebookUserEntry.COLUMN_NAME_ID + " WHERE f." + 
					FacebookUserEntry.COLUMN_NAME_FACEBOOK_ID + "=?");
			
			getUserInformationQuery.setString(1, fbResponse.getUserID());
			ResultSet getUserInformationResult = getUserInformationQuery.executeQuery();
			
			String generatedToken = LoginRequest.generateToken();
			
			if(!getUserInformationResult.next()) { 
				PreparedStatement newUserQuery = conn.prepareStatement("INSERT INTO " + BaseUserEntry.TABLE_NAME + " (" + BaseUserEntry.COLUMN_NAME_TOKEN + ") VALUES (?)");
				newUserQuery.setString(1, generatedToken);
				newUserQuery.executeUpdate();
				
				PreparedStatement getNewUserIDQuery = conn.prepareStatement("SELECT LAST_INSERT_ID() AS " + DatabaseContract.COLUMN_NAME_LAST_ID);
				ResultSet getNewUserIDQueryResult = getNewUserIDQuery.executeQuery();
				
				if(!getNewUserIDQueryResult.next())
					return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
				
				/*
				PreparedStatement newUserCache = conn.prepareStatement("INSERT INTO " + UserCacheEntry.TABLE_NAME + " (" + UserCacheEntry.COLUMN_NAME_USER_ID + ") VALUES (?)");
				newUserCache.setInt(1, getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				newUserCache.executeUpdate();
				*/
				
				PreparedStatement newFBUserQuery = conn.prepareStatement("INSERT INTO " + 
						FacebookUserEntry.TABLE_NAME + " (" + 
						FacebookUserEntry.COLUMN_NAME_ID + ", " + 
						FacebookUserEntry.COLUMN_NAME_FACEBOOK_ID + ") VALUES (?, ?)");
				
				newFBUserQuery.setInt(1, getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				newFBUserQuery.setString(2, fbResponse.getUserID());
				newFBUserQuery.executeUpdate();
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.NAME_CONF_REQUIRED.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getNewUserIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				if(fbResponse.getFirstName() != null)
					retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), fbResponse.getFirstName());
				if(fbResponse.getLastName() != null)
					retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), fbResponse.getLastName());
			} else if(getUserInformationResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED)) {	
				updateToken(getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID), generatedToken);
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_FIRST_NAME));
				retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_LAST_NAME));
				retValue.put(JSONIdentifier.USERNAME.getIdentifier(), getUserInformationResult.getString(BaseUserEntry.COLUMN_NAME_USERNAME));
			} else if(!getUserInformationResult.getBoolean(BaseUserEntry.COLUMN_NAME_NAME_CONFIRMED)) {
				updateToken(getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID), generatedToken);
				
				retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.NAME_CONF_REQUIRED.getCode());
				retValue.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getUserInformationResult.getInt(BaseUserEntry.COLUMN_NAME_ID));
				retValue.put(JSONIdentifier.GENERAL_TOKEN.getIdentifier(), generatedToken);
				if(fbResponse.getFirstName() != null)
					retValue.put(JSONIdentifier.FIRST_NAME.getIdentifier(), fbResponse.getFirstName());
				if(fbResponse.getLastName() != null)
					retValue.put(JSONIdentifier.LAST_NAME.getIdentifier(), fbResponse.getLastName());
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[FacebookLoginExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
	
	/**
	 * Controlla la veridicita dei dati inviati dal client effettuando una chiamata alle API Facebook.
	 * @param token AccessToken di facebook dell'utente richiedente.
	 * @param id UserID di facebook (relativo all'applicazione) dell'utente richiedente.
	 * @return Restituisce una istanza di FacebookResponse con Nome e Cognome dell'utente in caso di autenticazione riuscita, altrimenti un oggetto vuoto (vedi doc di FacebookResponse).
	 * @throws MalformedURLException Eccezione lanciata in caso di URL mal formato.
	 * @throws IOException Eccezione lanciata in caso di errore di comunicazione con i server facebook.
	 * @throws FacebookResponseException Eccezione lanciata in caso di autenticazione fallita.
	 */
	public static FacebookResponse checkFacebookData(final String token) {
		HttpsURLConnection facebookConnection = null;
		try {
			final URL facebookURL = new URL("https://graph.facebook.com/me?fields=id,first_name,last_name&access_token=" + token + "&appsecret_proof=" + getApplicationSecretProof(token));
			facebookConnection = (HttpsURLConnection)facebookURL.openConnection();
			facebookConnection.setRequestMethod("GET");
			facebookConnection.setUseCaches(false);
			facebookConnection.setDoOutput(false);
			facebookConnection.connect();

			if (facebookConnection.getResponseCode() != HttpURLConnection.HTTP_OK) {
				return new FacebookResponse(ErrorCode.INVALID_FACEBOOK_LOGIN.getCode());
			}

			final InputStream is = facebookConnection.getInputStream();
            final BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            final StringBuilder response = new StringBuilder();
            String line;

            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }
            rd.close();
            facebookConnection.disconnect();

            final JSONObject jsonObject = new JSONObject(response.toString());
            if(!jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier())) {
            	return new FacebookResponse(ErrorCode.SERVER_FACEBOOK_REPLY_ERROR.getCode());
            }

            final String userID = jsonObject.getString(JSONIdentifier.GENERAL_ID.getIdentifier());
            final String firstName = (jsonObject.has(JSONIdentifier.FIRST_NAME.getIdentifier()) ? jsonObject.getString(JSONIdentifier.FIRST_NAME.getIdentifier()) : null);
            final String lastName = (jsonObject.has(JSONIdentifier.LAST_NAME.getIdentifier()) ? jsonObject.getString(JSONIdentifier.LAST_NAME.getIdentifier()) : null);

            return new FacebookResponse(userID, firstName, lastName);
		} catch (final ParseException | AppSecretProofException e) {
			return new FacebookResponse(ErrorCode.SERVER_FACEBOOK_REPLY_ERROR.getCode());
		} catch (final IOException e) {
			return new FacebookResponse(ErrorCode.SERVER_FACEBOOK_CONNECTION_ISSUE.getCode());
		} finally {
			if(facebookConnection != null)
				facebookConnection.disconnect();
		}
	}

	/**
	 * Genera l'AppSecretProof da utilizzare per le chiamate ai server facebook come ulteriore forma di sicurezza.
	 * @param token Il token di facebook dell'utente che sta effettuando la richiesta. Viene utilizzato per generare il SecretProof.
	 * @return Restituisce la stringa da utilizzare per le chiamate alle api facebook.
	 * @throws AppSecretProofException Eccezione lanciata nel caso fosse impossibile generare l'App Secret Proof.
	 */
	private static String getApplicationSecretProof(final String token) throws AppSecretProofException {
		final StringBuilder sb = new StringBuilder();
		try {
		    Mac mac = Mac.getInstance("HmacSHA256");
		    SecretKeySpec secret = new SecretKeySpec(APP_SECRET_FACEBOOK.getBytes(),"HmacSHA256");
		    mac.init(secret);
		    byte[] digest = mac.doFinal(token.getBytes());

		    for (byte b : digest) {
		    	sb.append(String.format("%02x", b));
		    }
		} catch (Exception e) {
		    throw new AppSecretProofException();
		}

		return sb.toString();
	}
}
