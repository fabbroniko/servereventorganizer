package evento.executor.group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.GroupAuthorizationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.UserGroupEntry;

public class GroupMembersExecutor extends GroupAuthorizationRequired {

	private int id;
	private String token;
	private int groupID;
	
	public GroupMembersExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.GROUP_ID.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				groupID = jsonObject.getInt(JSONIdentifier.GROUP_ID.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserGroupAssociation(id, groupID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_USER_GROUP_ASSOCIATION.getCode());
			}
			
			PreparedStatement getMembersList = conn.prepareStatement("SELECT " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + ", " +
					DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", " +
					DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME + ", " +
					DatabaseContract.BaseUserEntry.COLUMN_NAME_USERNAME + " FROM " + 
					DatabaseContract.BaseUserEntry.TABLE_NAME + " WHERE " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + " IN (SELECT " + 
					UserGroupEntry.COLUMN_NAME_USER_ID + " FROM " + 
					UserGroupEntry.TABLE_NAME + " WHERE " + 
					UserGroupEntry.COLUMN_NAME_GROUP_ID + "=?)");
			getMembersList.setInt(1, groupID);
			ResultSet getMembersListResult = getMembersList.executeQuery();
			
			final JSONArray jsonArray = new JSONArray();
			while(getMembersListResult.next()) {
				final JSONObject tmpObj = new JSONObject();
				tmpObj.put(JSONIdentifier.GENERAL_ID.getIdentifier(), getMembersListResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID));
				tmpObj.put(JSONIdentifier.FULL_NAME.getIdentifier(), getMembersListResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME) + " " + getMembersListResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME));
				tmpObj.put(JSONIdentifier.USERNAME.getIdentifier(), getMembersListResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_USERNAME));
				
				jsonArray.put(tmpObj);
			}
			
			retValue.put(JSONIdentifier.PARTICIPANTS.getIdentifier(), jsonArray);
		} catch (final SQLException e) {
			Log.getInstance().print("[GroupMembersExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
