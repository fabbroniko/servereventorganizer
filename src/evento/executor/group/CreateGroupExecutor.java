package evento.executor.group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.GroupCacheEntry;

public class CreateGroupExecutor extends AuthenticationRequired {

	private static final int MIN_GROUP_NAME_LENGTH = 1;
	private static final int MAX_GROUP_NAME_LENGTH = 100;
	
	private int id;
	private String token;
	private String groupName;
	
	public CreateGroupExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.GROUP_NAME.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				groupName = jsonObject.getString(JSONIdentifier.GROUP_NAME.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		if(!isGroupNameValid(groupName))
			return buildErrorResponse(ErrorCode.EVENT_NAME_FORMAT_NOT_VALID.getCode());
		
		return executeQuery();
	}
	
	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		int groupID = 0;

		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement insertGroupQuery = conn.prepareStatement("INSERT INTO " + 
					DatabaseContract.GroupEntry.TABLE_NAME + 
					" (" + DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_NAME + ") VALUES (?)");
			
			insertGroupQuery.setString(1, groupName);
			insertGroupQuery.executeUpdate();
			
			PreparedStatement getGroupIDQuery = conn.prepareStatement("SELECT LAST_INSERT_ID() AS " + DatabaseContract.COLUMN_NAME_LAST_ID);
			ResultSet getGroupIDQueryResult = getGroupIDQuery.executeQuery();

			if(!getGroupIDQueryResult.next())
				return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
			
			groupID = getGroupIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID);
			
			PreparedStatement newGroupCache = conn.prepareStatement("INSERT INTO " + GroupCacheEntry.TABLE_NAME + " (" + GroupCacheEntry.COLUMN_NAME_GROUP_ID + ") VALUES (?)");
			newGroupCache.setInt(1, groupID);
			newGroupCache.executeUpdate();
			
			PreparedStatement insertUserGroupAssociation = conn.prepareStatement("INSERT INTO " + 
					DatabaseContract.UserGroupEntry.TABLE_NAME + 
					" (" + DatabaseContract.UserGroupEntry.COLUMN_NAME_USER_ID + 
					", " + DatabaseContract.UserGroupEntry.COLUMN_NAME_GROUP_ID + 
					") VALUES (?, ?)");
			
			insertUserGroupAssociation.setInt(1, id);
			insertUserGroupAssociation.setInt(2, groupID);
			insertUserGroupAssociation.executeUpdate();
			
			retValue.put(JSONIdentifier.GROUP_ID.getIdentifier(), groupID);
		} catch (final SQLException e) {
			Log.getInstance().print("[CreateGroupExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
	
	private static boolean isGroupNameValid(final String value) {
		return (value != null) && (value.length() >= MIN_GROUP_NAME_LENGTH) && (value.length() <= MAX_GROUP_NAME_LENGTH);
	}
}
