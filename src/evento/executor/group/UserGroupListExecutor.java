package evento.executor.group;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.UserGroupEntry;

public class UserGroupListExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	
	public UserGroupListExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement getGroupList = conn.prepareStatement("SELECT " + 
					DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_ID + ", " +
					DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_NAME + " FROM " + 
					DatabaseContract.GroupEntry.TABLE_NAME + " WHERE " + 
					DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_ID + " IN (SELECT " + 
					UserGroupEntry.COLUMN_NAME_GROUP_ID + " FROM " + 
					UserGroupEntry.TABLE_NAME + " WHERE " + 
					UserGroupEntry.COLUMN_NAME_USER_ID + "=?)");
			getGroupList.setInt(1, id);
			ResultSet getGroupListResult = getGroupList.executeQuery();
			
			final JSONArray jsonArray = new JSONArray();
			while(getGroupListResult.next()) {
				final JSONObject tmpObj = new JSONObject();
				tmpObj.put(JSONIdentifier.GROUP_ID.getIdentifier(), getGroupListResult.getInt(DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_ID));
				tmpObj.put(JSONIdentifier.GROUP_NAME.getIdentifier(), getGroupListResult.getString(DatabaseContract.GroupEntry.COLUMN_NAME_GROUP_NAME));
				
				jsonArray.put(tmpObj);
			}
			
			retValue.put(JSONIdentifier.GROUP_LIST.getIdentifier(), jsonArray);
		} catch (final SQLException e) {
			Log.getInstance().print("[UserGroupListExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
