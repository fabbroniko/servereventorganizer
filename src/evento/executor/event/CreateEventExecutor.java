package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.EventCacheEntry;

public class CreateEventExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	private String eventName;
	
	private static final int MIN_EVENT_NAME_LENGTH = 1;
	private static final int MAX_EVENT_NAME_LENGTH = 100;
	
	public CreateEventExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_NAME.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventName = jsonObject.getString(JSONIdentifier.EVENT_NAME.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		if(!isEventNameValid(eventName))
			return buildErrorResponse(ErrorCode.EVENT_NAME_FORMAT_NOT_VALID.getCode());
		
		return executeQuery();
	}
	
	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		int eventID = 0;

		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement insertEventQuery = conn.prepareStatement("INSERT INTO " + 
					DatabaseContract.EventEntry.TABLE_NAME + 
					" (" + DatabaseContract.EventEntry.COLUMN_NAME_NAME + ") VALUES (?)");
			
			insertEventQuery.setString(1, eventName);
			insertEventQuery.executeUpdate();
			
			PreparedStatement getEventIDQuery = conn.prepareStatement("SELECT LAST_INSERT_ID() AS " + DatabaseContract.COLUMN_NAME_LAST_ID);
			ResultSet getEventIDQueryResult = getEventIDQuery.executeQuery();

			if(!getEventIDQueryResult.next())
				return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
				
			eventID = getEventIDQueryResult.getInt(DatabaseContract.COLUMN_NAME_LAST_ID);
			
			PreparedStatement newEventCache = conn.prepareStatement("INSERT INTO " + EventCacheEntry.TABLE_NAME + " (" + EventCacheEntry.COLUMN_NAME_EVENT_ID + ") VALUES (?)");
			newEventCache.setInt(1, eventID);
			newEventCache.executeUpdate();
			
			PreparedStatement insertUserEventAssociationQuery = conn.prepareStatement("INSERT INTO " + 
					DatabaseContract.UserEventEntry.TABLE_NAME + 
					" (" + DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + 
					", " + DatabaseContract.UserEventEntry.COLUMN_NAME_EVENT_ID + 
					") VALUES (?, ?)");
			
			insertUserEventAssociationQuery.setInt(1, id);
			insertUserEventAssociationQuery.setInt(2, eventID);
			insertUserEventAssociationQuery.executeUpdate();
			
			//final long updateTimestamp = System.currentTimeMillis();
			//updateUserCache(id, updateTimestamp, UserCacheEntry.COLUMN_NAME_EVENT_LIST);
			
			retValue.put(JSONIdentifier.EVENT_ID.getIdentifier(), eventID);
			retValue.put(JSONIdentifier.EVENT_NAME.getIdentifier(), eventName);
			//retValue.put(JSONIdentifier.LAST_UPDATE.getIdentifier(), updateTimestamp);
		} catch (final SQLException e) {
			Log.getInstance().print("[CreateEventExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
	
	private static boolean isEventNameValid(final String value) {
		return (value != null) && (value.length() >= MIN_EVENT_NAME_LENGTH) && (value.length() <= MAX_EVENT_NAME_LENGTH);
	}
}
