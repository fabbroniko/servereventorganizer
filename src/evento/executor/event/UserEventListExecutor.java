package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.UserEventEntry;

public class UserEventListExecutor extends AuthenticationRequired {

	private int id;
	private String token;
	
	public UserEventListExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}

		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}

		final JSONObject retValue = new JSONObject();

		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}

			PreparedStatement getEventList = conn.prepareStatement("SELECT " +
					DatabaseContract.EventEntry.COLUMN_NAME_ID + ", " +
					DatabaseContract.EventEntry.COLUMN_NAME_NAME + ", " +
					DatabaseContract.EventEntry.COLUMN_NAME_PLACE + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_HOUR + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_MINUTE + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_DESCRIPTION + " FROM " +
					DatabaseContract.EventEntry.TABLE_NAME + " WHERE " +
					DatabaseContract.EventEntry.COLUMN_NAME_ID + " IN (SELECT " + 
					UserEventEntry.COLUMN_NAME_EVENT_ID + " FROM " +
					UserEventEntry.TABLE_NAME + " WHERE " +
					UserEventEntry.COLUMN_NAME_USER_ID + "=?)");
			getEventList.setInt(1, id);
			ResultSet getEventListResult = getEventList.executeQuery();

			final JSONArray jsonArray = new JSONArray();
			while(getEventListResult.next()) {
				final JSONObject tmpObj = new JSONObject();
				tmpObj.put(JSONIdentifier.EVENT_ID.getIdentifier(), getEventListResult.getInt(DatabaseContract.EventEntry.COLUMN_NAME_ID));
				tmpObj.put(JSONIdentifier.EVENT_NAME.getIdentifier(), getEventListResult.getString(DatabaseContract.EventEntry.COLUMN_NAME_NAME));
				tmpObj.put(JSONIdentifier.PLACE.getIdentifier(), getEventListResult.getString(DatabaseContract.EventEntry.COLUMN_NAME_PLACE));
				tmpObj.put(JSONIdentifier.HOUR.getIdentifier(), getEventListResult.getInt(DatabaseContract.EventEntry.COLUMN_NAME_HOUR));
				tmpObj.put(JSONIdentifier.MINUTE.getIdentifier(), getEventListResult.getInt(DatabaseContract.EventEntry.COLUMN_NAME_MINUTE));
				tmpObj.put(JSONIdentifier.DESCRIPTION.getIdentifier(), getEventListResult.getString(DatabaseContract.EventEntry.COLUMN_NAME_DESCRIPTION));

				jsonArray.put(tmpObj);
			}

			retValue.put(JSONIdentifier.EVENT_LIST.getIdentifier(), jsonArray);
		} catch (final SQLException e) {
			Log.getInstance().print("[UserEventListExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}

		return retValue.toString();
	}

}
