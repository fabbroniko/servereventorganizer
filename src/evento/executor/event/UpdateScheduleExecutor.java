			package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.Log;
import evento.utils.DatabaseContract.EventEntry;

public class UpdateScheduleExecutor extends EventAuthorizationRequired{

	private int id;
	private String token;
	private int eventID;
	private int hour;
	private int minute;
	
	public UpdateScheduleExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.HOUR.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.MINUTE.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
				hour = jsonObject.getInt(JSONIdentifier.HOUR.getIdentifier());
				minute = jsonObject.getInt(JSONIdentifier.MINUTE.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			PreparedStatement updateQuery = conn.prepareStatement("UPDATE " + 
					EventEntry.TABLE_NAME + " SET " + 
					EventEntry.COLUMN_NAME_HOUR + "=?, " +
					EventEntry.COLUMN_NAME_MINUTE +  "=? WHERE " + 
					EventEntry.COLUMN_NAME_ID + "=?");
			
			updateQuery.setInt(1, hour);
			updateQuery.setInt(2, minute);
			updateQuery.setInt(3, eventID);
			updateQuery.executeUpdate();
		} catch (final SQLException e) {
			Log.getInstance().print("[UpdateScheduleExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		final JSONObject retValue = new JSONObject();
		retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
		
		return retValue.toString();
	}
}
