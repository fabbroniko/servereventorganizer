package evento.executor.event;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.Log;
import evento.utils.DatabaseContract.EventAvailabilityEntry;

public class UpdateDatesExecutor extends EventAuthorizationRequired {

	private int id;
	private String token;
	private int eventID;
	private List<Long> dates;
	
	public UpdateDatesExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.DATES.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
				final JSONArray jsonArray = jsonObject.getJSONArray(JSONIdentifier.DATES.getIdentifier());
				dates = new ArrayList<>();
				for(int i = 0; i < jsonArray.length(); i++) {
					dates.add(((Double)jsonArray.get(i)).longValue());
				}
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		if(!areDatesValid(dates))
			return buildErrorResponse(ErrorCode.DATES_NOT_VALID.getCode());
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			PreparedStatement deleteQuery = conn.prepareStatement("DELETE FROM " + 
					EventAvailabilityEntry.TABLE_NAME + " WHERE " + 
					EventAvailabilityEntry.COLUMN_NAME_EVENT_ID + "=? AND " + 
					EventAvailabilityEntry.COLUMN_NAME_USER_ID + "=?");
			
			deleteQuery.setInt(1, eventID);
			deleteQuery.setInt(2, id);
			deleteQuery.executeUpdate();
			
			for(final Long i : dates) {
				PreparedStatement insertQuery = conn.prepareStatement("INSERT INTO " + 
						EventAvailabilityEntry.TABLE_NAME + " (" + 
						EventAvailabilityEntry.COLUMN_NAME_USER_ID + ", " + 
						EventAvailabilityEntry.COLUMN_NAME_EVENT_ID + ", " + 
						EventAvailabilityEntry.COLUMN_NAME_DATE + ") VALUES (?, ?, ?)");
				
				insertQuery.setInt(1, id);
				insertQuery.setInt(2, eventID);
				insertQuery.setDate(3, new Date(i));
				insertQuery.executeUpdate();
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[UpdateDatesExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		final JSONObject retValue = new JSONObject();
		retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
		retValue.put(JSONIdentifier.EVENT_ID.getIdentifier(), eventID);
		
		return retValue.toString();
	}
	
	public static boolean areDatesValid(final List<Long> value) {
		return (value != null);
	}
}
