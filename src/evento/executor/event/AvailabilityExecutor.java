package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;
import evento.utils.DatabaseContract.BaseUserEntry;

public class AvailabilityExecutor extends EventAuthorizationRequired {

	private int id;
	private String token;
	private int eventID;
	
	public AvailabilityExecutor(String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {	
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			PreparedStatement datesQuery = conn.prepareStatement("SELECT " + 
					DatabaseContract.EventAvailabilityEntry.COLUMN_NAME_USER_ID + ", " + 
					DatabaseContract.EventAvailabilityEntry.COLUMN_NAME_DATE + " FROM " + 
					DatabaseContract.EventAvailabilityEntry.TABLE_NAME + " WHERE " + 
					DatabaseContract.EventAvailabilityEntry.COLUMN_NAME_EVENT_ID + "=?");
			
			datesQuery.setInt(1, eventID);
			ResultSet datesQueryResult = datesQuery.executeQuery();
			
			JSONArray jsonArray = new JSONArray();
			while(datesQueryResult.next()) {
				JSONObject jsonRow = new JSONObject();
				jsonRow.put(JSONIdentifier.GENERAL_ID.getIdentifier(), datesQueryResult.getInt(DatabaseContract.EventAvailabilityEntry.COLUMN_NAME_USER_ID));
				jsonRow.put(JSONIdentifier.DATE.getIdentifier(), datesQueryResult.getDate(DatabaseContract.EventAvailabilityEntry.COLUMN_NAME_DATE).getTime());
				jsonArray.put(jsonRow);
			}
			
			PreparedStatement nameQuery = conn.prepareStatement("SELECT bu." + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + ", bu." + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME + ", bu." + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME + ", bu. " + 
					DatabaseContract.BaseUserEntry.COLUMN_NAME_USERNAME + " FROM " + 
					DatabaseContract.BaseUserEntry.TABLE_NAME + " bu JOIN " + 
					DatabaseContract.UserEventEntry.TABLE_NAME + " ue ON bu." +
					DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + "=ue." + 
					DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + " WHERE " + 
					DatabaseContract.UserEventEntry.COLUMN_NAME_EVENT_ID + "=?");
			
			nameQuery.setInt(1, eventID);
			ResultSet nameQueryResult = nameQuery.executeQuery();
			
			JSONArray nameArray = new JSONArray();
			while(nameQueryResult.next()) {
				String tmpUserName = nameQueryResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_FIRST_NAME) + " " + nameQueryResult.getString(DatabaseContract.BaseUserEntry.COLUMN_NAME_LAST_NAME);
				JSONObject jsonRow = new JSONObject();
				jsonRow.put(JSONIdentifier.GENERAL_ID.getIdentifier(), nameQueryResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID));
				jsonRow.put(JSONIdentifier.FULL_NAME.getIdentifier(), tmpUserName);
				jsonRow.put(JSONIdentifier.USERNAME.getIdentifier(), nameQueryResult.getString(BaseUserEntry.COLUMN_NAME_USERNAME));
				nameArray.put(jsonRow);
			}
			
			retValue.put(JSONIdentifier.DATES.getIdentifier(), jsonArray);
			retValue.put(JSONIdentifier.PARTICIPANTS.getIdentifier(), nameArray);
		} catch (final SQLException e) {
			Log.getInstance().print("[AvailabilityExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
