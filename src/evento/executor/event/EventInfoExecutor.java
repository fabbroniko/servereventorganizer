package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public class EventInfoExecutor extends EventAuthorizationRequired {

	private int id;
	private String token;
	private int eventID;
	
	public EventInfoExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}	
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			PreparedStatement getInfoQuery = conn.prepareStatement("SELECT " + 
					DatabaseContract.EventEntry.COLUMN_NAME_PLACE + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_HOUR + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_MINUTE + ", " + 
					DatabaseContract.EventEntry.COLUMN_NAME_DESCRIPTION + " FROM " + DatabaseContract.EventEntry.TABLE_NAME + 
					" WHERE " + DatabaseContract.EventEntry.COLUMN_NAME_ID + "=?");
			getInfoQuery.setInt(1, eventID);
			ResultSet getInfoQueryResult = getInfoQuery.executeQuery();
			
			if(getInfoQueryResult.next()) {
				retValue.put(JSONIdentifier.PLACE.getIdentifier(), getInfoQueryResult.getString(DatabaseContract.EventEntry.COLUMN_NAME_PLACE));
				retValue.put(JSONIdentifier.HOUR.getIdentifier(), getInfoQueryResult.getInt(DatabaseContract.EventEntry.COLUMN_NAME_HOUR));
				retValue.put(JSONIdentifier.MINUTE.getIdentifier(), getInfoQueryResult.getInt(DatabaseContract.EventEntry.COLUMN_NAME_MINUTE));
				retValue.put(JSONIdentifier.DESCRIPTION.getIdentifier(), getInfoQueryResult.getString(DatabaseContract.EventEntry.COLUMN_NAME_DESCRIPTION));
			} else {
				return buildErrorResponse(ErrorCode.EVENT_ID_DOESNT_EXIST.getCode());
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[EventInfoExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		return retValue.toString();
	}
}
