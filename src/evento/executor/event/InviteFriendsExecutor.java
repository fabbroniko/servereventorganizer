package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public class InviteFriendsExecutor extends EventAuthorizationRequired {

	private int id;
	private String token;
	private int eventID;
	private List<String> usernameList;
	
	public InviteFriendsExecutor(final String jsonResponse) {
		super(jsonResponse);
	}
	
	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.USERNAME_LIST.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
				final JSONArray ja = jsonObject.getJSONArray(JSONIdentifier.USERNAME_LIST.getIdentifier());
				usernameList = new ArrayList<>();
				for(int i = 0; i < ja.length(); i++) {
					usernameList.add(ja.getString(i));
				}
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			for(final String username : usernameList) {
				PreparedStatement getUsernameID = conn.prepareStatement("SELECT @username_id:=" + 
						DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + " AS " + DatabaseContract.BaseUserEntry.COLUMN_NAME_ID + 
						" FROM " + DatabaseContract.BaseUserEntry.TABLE_NAME + 
						" WHERE " + DatabaseContract.BaseUserEntry.COLUMN_NAME_USERNAME + "=?");
				getUsernameID.setString(1, username);
				ResultSet getUsernameIDResult = getUsernameID.executeQuery();
			
				if(getUsernameIDResult.next() && id != getUsernameIDResult.getInt(DatabaseContract.BaseUserEntry.COLUMN_NAME_ID)) {
					PreparedStatement insertUserEventAssociationQuery = conn.prepareStatement("INSERT INTO " + 
								DatabaseContract.UserEventEntry.TABLE_NAME + 
								" (" + DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + 
								", " + DatabaseContract.UserEventEntry.COLUMN_NAME_EVENT_ID + 
								") VALUES (@username_id, ?) ON DUPLICATE KEY UPDATE " + 
								DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + "=@username_id");
				
					insertUserEventAssociationQuery.setInt(1, eventID);
					insertUserEventAssociationQuery.executeUpdate();
				}
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[InviteFriendExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
		
		return retValue.toString();
	}
}
