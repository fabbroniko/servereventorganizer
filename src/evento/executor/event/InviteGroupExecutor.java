package evento.executor.event;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.EventAuthorizationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public class InviteGroupExecutor extends EventAuthorizationRequired {

	private int id;
	private String token;
	private int eventID;
	private int groupID;
	
	public InviteGroupExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.EVENT_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GROUP_ID.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				eventID = jsonObject.getInt(JSONIdentifier.EVENT_ID.getIdentifier());
				groupID = jsonObject.getInt(JSONIdentifier.GROUP_ID.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		return executeQuery();
	}

	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			if(!checkUserEventAssociation(id, eventID)) {
				return buildErrorResponse(ErrorCode.NO_SUCH_EVENT_USER_ASSOC.getCode());
			}
			
			PreparedStatement getUserIDList = conn.prepareStatement("SELECT " + 
					DatabaseContract.UserGroupEntry.COLUMN_NAME_USER_ID + " FROM " + 
					DatabaseContract.UserGroupEntry.TABLE_NAME + " WHERE " + 
					DatabaseContract.UserGroupEntry.COLUMN_NAME_GROUP_ID + "=?");
			getUserIDList.setInt(1, groupID);
			ResultSet getUserIDListResult = getUserIDList.executeQuery();
			
			while(getUserIDListResult.next()) {
				int currentUserID = getUserIDListResult.getInt(DatabaseContract.UserGroupEntry.COLUMN_NAME_USER_ID);
				PreparedStatement insertUserEventAssociationQuery = conn.prepareStatement("INSERT INTO " + 
						DatabaseContract.UserEventEntry.TABLE_NAME + 
						" (" + DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + 
						", " + DatabaseContract.UserEventEntry.COLUMN_NAME_EVENT_ID + 
						") VALUES (?, ?) ON DUPLICATE KEY UPDATE " + 
						DatabaseContract.UserEventEntry.COLUMN_NAME_USER_ID + "=?");
				
				insertUserEventAssociationQuery.setInt(1, currentUserID);
				insertUserEventAssociationQuery.setInt(2, eventID);
				insertUserEventAssociationQuery.setInt(3, currentUserID);
				insertUserEventAssociationQuery.executeUpdate();
			}
		} catch (final SQLException e) {
			Log.getInstance().print("[InviteGroupExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS.getCode());
		
		return retValue.toString();
	}

}
