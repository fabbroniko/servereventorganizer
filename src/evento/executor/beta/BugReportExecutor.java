package evento.executor.beta;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import org.json.JSONObject;

import evento.enums.Code;
import evento.enums.ErrorCode;
import evento.enums.JSONIdentifier;
import evento.exception.DatabaseConnectionException;
import evento.exception.NameNotConfirmedException;
import evento.executor.AuthenticationRequired;
import evento.utils.DatabaseContract;
import evento.utils.Log;

public class BugReportExecutor extends AuthenticationRequired {

	private static final int MIN_VALUE = 0;
	private static final int MAX_TYPE_VALUE = 10;
	private static final int MAX_FRAGMENT_VALUE = 20;
	
	private int id;
	private String token;
	private int type;
	private int fragment;
	private String details;
	
	public BugReportExecutor(final String jsonRequest) {
		super(jsonRequest);
	}

	@Override
	public String execute() {
		if(jsonRequest == null)
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		try {
			final JSONObject jsonObject = new JSONObject(jsonRequest);
			if(jsonObject.has(JSONIdentifier.GENERAL_ID.getIdentifier()) && 
					jsonObject.has(JSONIdentifier.GENERAL_TOKEN.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.BUG_TYPE.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.BUG_FRAGMENT.getIdentifier()) &&
					jsonObject.has(JSONIdentifier.BUG_DETAILS.getIdentifier())) {
				
				id = jsonObject.getInt(JSONIdentifier.GENERAL_ID.getIdentifier());
				token = jsonObject.getString(JSONIdentifier.GENERAL_TOKEN.getIdentifier());
				type = jsonObject.getInt(JSONIdentifier.BUG_TYPE.getIdentifier());
				fragment = jsonObject.getInt(JSONIdentifier.BUG_FRAGMENT.getIdentifier());
				details = jsonObject.getString(JSONIdentifier.BUG_DETAILS.getIdentifier());
			} else {
				return buildErrorResponse(ErrorCode.MISSING_FIELD.getCode());
			}
		} catch (final ParseException e) {
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		}
		
		if(!isTypeValid(type))
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		if(!isFragmentValid(fragment))
			return buildErrorResponse(ErrorCode.BAD_REQUEST.getCode());
		
		return executeQuery();
	}
	
	private String executeQuery() {
		try {
			connect();
		} catch (DatabaseConnectionException e) {
			return buildErrorResponse(ErrorCode.SERVER_DATABASE_CONNECTION_FAILED.getCode());
		}
		
		final JSONObject retValue = new JSONObject();
		
		try {	
			if(!authenticate(id, token)) {
				return buildErrorResponse(ErrorCode.AUTH_FAILED.getCode());
			}
			
			PreparedStatement insertQuery = conn.prepareStatement("INSERT INTO " + 
					DatabaseContract.BugReport.TABLE_NAME + " (" +
					DatabaseContract.BugReport.COLUMN_NAME_USER_ID + ", " +
					DatabaseContract.BugReport.COLUMN_NAME_TYPE + ", " + 
					DatabaseContract.BugReport.COLUMN_NAME_FRAGMENT + ", " + 
					DatabaseContract.BugReport.COLUMN_NAME_DETAILS + ") VALUES (?, ?, ?, ?)");
			
			insertQuery.setInt(1, id);
			insertQuery.setInt(2, type);
			insertQuery.setInt(3, fragment);
			insertQuery.setString(4, details);
			insertQuery.executeUpdate();
		} catch (final SQLException e) {
			Log.getInstance().print("[BugReportExecutor] - " + e.getMessage() + ((e.getStackTrace() == null) ? "" : " - Stack trace: " + e.getStackTrace().toString()));
			return buildErrorResponse(ErrorCode.SERVER_SQL_EXCEPTION.getCode());
		} catch (final NameNotConfirmedException e) {
			return buildErrorResponse(ErrorCode.NAME_NOT_CONFIRMED.getCode());
		} finally {
			disconnect();
		}
		
		retValue.put(JSONIdentifier.CODE.getIdentifier(), Code.SUCCESS);
		return retValue.toString();
	}
	
	private boolean isTypeValid(final int type) {
		return (type >= MIN_VALUE) && (type <= MAX_TYPE_VALUE);
	}
	
	private boolean isFragmentValid(final int fragment) {
		return (fragment >= MIN_VALUE) && (fragment <= MAX_FRAGMENT_VALUE);
	}
}
