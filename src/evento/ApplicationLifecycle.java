package evento;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import evento.utils.Log;

/**
 * Listener che permette all'applicazione di determinare quando questa viene avviata e distrutta.
 * Quando viene esportata la WebApplication � necessario:
 * - Impostare il Logging su terminale a false nella classe Log
 * - Impostare la password del database MySQL.
 * Dopo l'export:
 * - Eliminare password dal sorgente.
 * Per il debugging locale:
 * - Impostare il Logging su terminale a true nella classe Log
 * - Impostare la password del database locale (sharedwithfede)
 * @author fabbroniko
 *
 */
@WebListener
public class ApplicationLifecycle implements ServletContextListener, Runnable {

	private boolean isRunning = false;
	private Thread dbManagerThread = null; // DBMT
	
	private static final int DBMT_SLEEP_TIME = (1000 * 60 * 60 * 24) + 1000;

	@Override
	public void contextInitialized(final ServletContextEvent arg0) {
		Log.getInstance().print("Web application started.");
		
		if(isRunning || dbManagerThread != null)
			contextDestroyed(arg0);
			
		isRunning = true;
		dbManagerThread = new Thread(this);
		dbManagerThread.start();
	}
	
	@Override
	public void contextDestroyed(final ServletContextEvent arg0) {
		Log.getInstance().print("Closing the web application...");
		isRunning = false;
		if(dbManagerThread != null) {
			try {
				dbManagerThread.interrupt();
				dbManagerThread.join();
			} catch (InterruptedException e) {
				Log.getInstance().print("Unable to join the DBMT.");
				e.printStackTrace();
			}
		}
		dbManagerThread = null;
	}

	@Override
	public void run() {
		while(isRunning) {
			Log.getInstance().print("Starting a new DBMT cycle...");
			
			// TODO operazioni da svolgere nel manager. Un esempio potrebbe essere la rimozione delle date precedenti alla data attuale del server.
			
			try {
				Thread.sleep(DBMT_SLEEP_TIME);
			} catch (final InterruptedException e) {
				Log.getInstance().print("DBMT interrupted.");
			}
		}
		Log.getInstance().print("DBMT closed.");
	}
}
