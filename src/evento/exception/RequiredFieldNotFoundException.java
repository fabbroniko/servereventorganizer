package evento.exception;

public class RequiredFieldNotFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "Uno o pi� campi obbligatori non sono presenti.";
	}
}
