package evento.exception;

public class AppSecretProofException extends Exception {

	private static final long serialVersionUID = 1L;

	@Override
	public String toString() {
		return "Impossibile generare l'app secret proof per le query alle api facebook.";
	}
}
