package evento.exception;

public class DatabaseConnectionException extends Exception {

private static final long serialVersionUID = 1L;
	
	@Override
	public String toString() {
		return "Impossibile stabilire una connessione con il database.";
	}
}
