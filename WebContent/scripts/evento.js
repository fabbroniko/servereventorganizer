var menuHandler = (function() {

  return {
    showCompleteMenu: function(show) {
      $(".menu_all_users").show();
      if(show) {
        $(".menu_logged_users_only").show();
        $(".menu_not_logged_users_only").hide();
      } else {
        $(".menu_logged_users_only").hide();
        $(".menu_not_logged_users_only").show();
      }
    }
  };
})();

var contentHandler = (function() {

  function hideContent() {
    $("#evento-home").hide();
    $("#evento-login").hide();
    $("#evento-events").hide();
    $("#evento-groups").hide();
    $("#evento-friends").hide();
    $("#evento-about").hide();
    $("#evento-contact").hide();
  }

  return {
    showLogin: function() {
      hideContent();
      $("#evento-login").show();
    },

    showMainPage: function() {
      hideContent();
      $("#evento-home").show();
    },

    showEvents: function() {
      hideContent();
      $("#evento-events").show();
    },

    showGroups: function() {
      hideContent();
      $("#evento-groups").show();
    },

    showFriends: function() {
      hideContent();
      $("#evento-friends").show();
    },

    showContact: function() {
      hideContent();
      $("#evento-contact").show();
    },

    showAbout: function() {
      hideContent();
      $("#evento-about").show();
    }
  };
})();

var login = (function() {

  var userToken = null;
  var userID = -1

  function performFacebookLogin(response) {
    /* TODO Complete the authentication with facebook.
     * Handles the errors and fills the properties with the returned userID and userToken.
     */
     if (response.status === 'connected') {
       // Logged into your app and Facebook.
      console.log("The user is logged into facebook-evento.");
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      console.log("The user is logged with facebook but not in evento.")
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      console.log("The user is not logged with facebook.");
    }
  }

  return {
    autoLogin: function() {
      /* TODO Check if the properties have been set.
       * Check if the user has cookies for the auto-login feature
       * If present, check the id and token and complete the auto-login
       * Also check if the user is already logged in with facebook or google.
       */
      FB.getLoginStatus(function(response) {
        performFacebookLogin(response);
      });
    },

    facebookLogin: function() {

    },

    googleLogin: function() {

    },

    isUserLogged: function() {
      return (userToken != null) && (userID != -1);
    },

    logout: function() {
      userToken = null;
      userID = -1;

      // TODO Logout from facebook
      // TODO Logout from google
    },

    debuggingLogin: function() {
      userToken = "DEBUGGING_TOKEN";
      userID = 1;
      return true;
    }
  };
})();

$(document).ready(function() {
  menuHandler.showCompleteMenu(login.isUserLogged());

  // Handling the menu links
  $("#evento-nav-login").click(function() {
    contentHandler.showLogin();
  });
  $("#evento-nav-header").click(function() {
    contentHandler.showMainPage();
  });
  $("#evento-nav-events").click(function() {
    contentHandler.showEvents();
  });
  $("#evento-nav-groups").click(function() {
    contentHandler.showGroups();
  });
  $("#evento-nav-friends").click(function() {
    contentHandler.showFriends();
  });
  $("#evento-nav-about").click(function(){
    contentHandler.showAbout();
  });
  $("#evento-nav-contact").click(function() {
    contentHandler.showContact();
  });
  $("#evento-nav-logout").click(function() {
    login.logout();
    menuHandler.showCompleteMenu(false);
    contentHandler.showMainPage();
  });

  // Handling the login content
  $("#evento-debugging-login-button").click(function() {
    if(login.debuggingLogin()) {
      contentHandler.showEvents();
      menuHandler.showCompleteMenu(true);
    }
  });
});
